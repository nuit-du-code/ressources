# FEUILLE DE ROUTE

- Créer des ressources pédagogique "Pyxel / Nuit du Code" pour les cours de NSI 1re et Tle
- Écrire des tutoriels pour Pyxel
- Actualiser les tutoriels vidéo Scratch
- Créer des tutoriels vidéo Pyxel / Python
